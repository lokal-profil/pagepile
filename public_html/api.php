<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

set_time_limit ( 60 * 30 ) ; // 30 Minutes max runtime

require_once ( 'php/common.php' ) ;
require_once ( 'php/pagepile.php' ) ;
require_once ( 'php/oauth.php' ) ;


function outputDataHTML ( $pp ) {
	global $id ;
	$start = get_request ( 'start' , 0 ) * 1 ;
	$max = get_request ( 'max' , 100 ) * 1 ;
	$total = $pp->getPageNumber() ;
	$wiki = $pp->getWiki() ;
	header('Content-Type: text/html');
	print "<!DOCTYPE html>\n<html><head><meta charset='utf-8' /></head><body>" ;
	print "<script src='https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/2.1.4/jquery.min.js'></script>" ;
	print "<p><a href='/pagepile'>Pile</a> <b>$id</b>: " . number_format($total) . " pages on $wiki. Other formats: 
	<a download='pagepile_$id.txt' href='?id=$id&action=get_data&format=text&doit'>Plain text</a>, 
	<a download='pagepile_wiki_$id.txt' href='?id=$id&action=get_data&format=wiki&doit'>wikitext</a>, 
	<a download='pagepile_json_$id.json' href='?id=$id&action=get_data&format=json&doit'>JSON</a>,
	<a download='pagepile_json_meta_$id.json' href='?id=$id&action=get_data&format=json&metadata=1&doit'>JSON (with metadata)</a>
	</p>" ;
	print "<p>" . $pp->getConsumerHTML() . "</p>" ;
	print "<hr/>" ;
	
	$nav = '' ;
	if ( $start > 0 or $start+$max+1 < $total ) {
		$nav .= "<div>" ;
		if ( $start > 0 ) {
			$s0 = $start - $max ;
			if ( $s0 < 0 ) $s0 = 0 ;
			if ( $s0 > 0 ) $nav .= "<a href='?id=$id&action=get_data&format=html&start=0&max=$max'>Start</a> | " ;
			
			$nav .= "<a href='?id=$id&action=get_data&format=html&start=$s0&max=$max'>" . ($s0+1) . "&ndash;" . ($s0+$max) . "</a> | " ;
		}
		
		$nav .= "<b>" . ($start+1) . "&ndash;" . ($start+$max) . "</b>" ;
		
		if ( $start+$max+1 < $total ) {
			$s0 = $start + $max ;
			while ( $s0 > $total ) $s0-- ;
			$nav .= " | <a href='?id=$id&action=get_data&format=html&start=$s0&max=$max'>" . ($s0+1) . "&ndash;" . ($s0+$max) . "</a>" ;
			
			if ( $s0 < $total ) $nav .= " | <a href='?id=$id&action=get_data&format=html&start=" . ($total-$max) . "&max=$max'>End</a>" ;
		}
		
#		$nav .= " | Total: $total pages" ;
		
		$nav .= "</div>" ;
	}
	
	
	print $nav ;
	print "<ol start='" . ($start+1) . "'>\n" ;
	$a = $pp->getArray ( $start , $max ) ;

	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	foreach ( $a AS $o ) {
		print "<li>" ;
		print $pp->getPageLink ( $o ) ;
		if ( $wiki == 'wikidatawiki' and $o->ns == 0 ) {
			$sql = "SELECT * FROM wb_terms WHERE term_entity_id=" . preg_replace('/\D/','',$o->page) . " AND term_entity_type='item' AND term_language='en' AND term_type='label' LIMIT 1" ;
			if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
			if($r = $result->fetch_object()){
				print " " . $r->term_text ;
			}
		}
		print "</li>\n" ;
	}
//	} ) ;
	print "</ol>" ;
	print $nav ;
	
	
	$url = "https://tools.wmflabs.org/pagepile/api.php?action=get_data&format=html&id=" . get_request('id','') ;
	print "<p><small>URL:<br/><a href='$url'>$url</a>" . "</small></p>" ;
	
	print "<hr/><div>" . $pp->getAutoDesc() . "</div>" ;
	
	print "</body></html>" ;
	exit ( 0 ) ;
}

function outputDataText ( $pp ) {
	header('Content-Type: text/plain; charset=utf-8');
	$pp->each ( function ( $o , $pp ) {
		print str_replace('_',' ',$pp->getFullPageTitle($o->page,$o->ns)) . "\n" ;
	} ) ;
	exit ( 0 ) ;
}

function outputDataWiki ( $pp ) {
	header('Content-Type: text/plain; charset=utf-8');
	$pp->each ( function ( $o , $pp ) {
		print "* [[" . str_replace('_',' ',$pp->getFullPageTitle($o->page,$o->ns)) . "]]\n" ;
	} ) ;
	$url = "https://tools.wmflabs.org/pagepile/api.php?action=get_data&format=wiki&id=" . get_request('id','') ;
	print "[$url Source]\n" ;
	exit ( 0 ) ;
}

function outputDataJSON ( $pp ) {
	global $id ;
	header('Content-Type: application/json; charset=utf-8');
	if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback']."(" ;
	$metadata = isset($_REQUEST['metadata']) ;
	print '{"pages":' ;
	print ($metadata?'{':'[') ;
	$first = true ;
	$pp->each ( function ( $o , $pp ) use (&$first,$metadata) {
		if ( $first ) { $first = false ; }
		else { print "," ; }
		$title = $pp->getFullPageTitle($o->page,$o->ns) ;
		if ( $metadata ) {
			print json_encode ( $title ) . ':' ;
			print json_encode ( (object) ( json_decode($o->json) ) ) ;
		}else print json_encode ( $title ) ;
	} ) ;
	print ($metadata?'}':']') ;
	print ',"wiki":' . json_encode($pp->getWiki()) . ',"id":' . $id . '}' ;
	if ( isset($_REQUEST['callback']) ) print ");" ;
	exit ( 0 ) ;
}

$action = get_request ( 'action' , '' ) ;
$out = array ( 'status' => 'OK' ) ;
$id = get_request ( 'id' , '' ) ;

if ( $action == 'create_pile' ) {

	if ( $oauth_pagepile->is_authorized ) $user = $oauth_pagepile->username ;
	else $user = 'Anonymous user' ;
	$pp = new PagePile ;
	$pp->getNewSqliteFile ( $user ) ;
	$out['pile']['id'] = $pp->getPileID() ;
	$out['pile']['file'] = $pp->getSqliteFile() ;

} else if ( $action == 'get_info' ) {

	if ( !is_numeric($id) ) {
		$out['status'] = "ERROR: No ID given for $action" ;
	} else {
		$pp = new PagePile ( $id ) ;
		$out['pile']['id'] = $pp->getPileID() ;
		$out['pile']['file'] = $pp->getSqliteFile() ;
	}


} else if ( $action == 'get_data' ) {

	$id = get_request ( 'id' , '' ) ;
	$format = get_request ( 'format' , 'html' ) ;
	if ( !is_numeric($id) ) {
		$out['status'] = "ERROR: No ID given for $action" ;
	} else {
		$pp = new PagePile ( $id ) ;
		
		if ( $format == 'html' ) outputDataHTML($pp) ;
		else if ( $format == 'text' ) outputDataText($pp) ;
		else if ( $format == 'wiki' ) outputDataWiki($pp) ;
		else if ( $format == 'json' ) outputDataJSON($pp) ;
		else $out['status'] = "ERROR: Unknown format '$format'" ;
		
	}


} else if ( $action == 'run_filters' ) {

	set_time_limit ( 60 * 20 ) ; // 20 minutes
	$pp = '' ;
	
	$out['log'] = array() ;
	$filters = json_decode ( get_request ( 'filters' , '[]' ) ) ;
	
	function addLog ( $msg ) {
		global $pp , $out ;
		$pid = $pp->getPileID() ;
		$omsg = $msg ;
		$msg = "<tr><td style='text-align:right;font-family:courier'>" . (count($out['log'])+1) . "</td><td>$msg</td>" ;
		$msg .= "<td>PagePile <a target='_blank' href='/pagepile/api.php?id=$pid&action=get_data&format=html&doit1'>#$pid</a></td>" ;
		$msg .= "<td style='text-align:right;font-family:courier'>" . number_format($pp->getPageNumber()) . " pages</td></tr>" ;
		$pp->addTrack ( array ( 'action' => 'filter' , 'label' => $omsg , 'ts' => $pp->getCurrentTimestamp() ) ) ;
		$out['log'][] = $msg ;
	}
	
	foreach ( $filters AS $f ) {

//		print "<pre>\n" ; print_r ( $f ) ; print "</pre>\n" ;  exit ( 0 ) ;

		$kv = (object) array() ;
		foreach ( $f->params AS $k => $v ) {
			$name = $v->name ;
			$kv->$name = $v->value ;
		}

		if ( $f->type == 'import' ) {
		
			$id = $f->params[0]->value ;
			$pp = new PagePile ( $id ) ;
			$pp = $pp->duplicate() ;
			addLog ( "Imported from PagePile #$id" ) ;
		
		} else if ( $f->type == 'union' ) {
		
			$id = $f->params[0]->value ;
			$pp->union ( $id ) ;
			addLog ( "Union with PagePile #$id" ) ;
		
		
		} else if ( $f->type == 'subset' ) {
		
			$id = $f->params[0]->value ;
			$pp->subset ( $id ) ;
			addLog ( "Subset with PagePile #$id" ) ;
		
		} else if ( $f->type == 'exclusive' ) {
		
			$id = $f->params[0]->value ;
			$pp->exclusive ( $id ) ;
			addLog ( "Exclusive with PagePile #$id" ) ;
		
		} else if ( $f->type == 'to_wikidata' ) {
		
			$msg = "Switched to Wikidata" ;
			if ( $pp->getWiki() != 'wikidatawiki' ) {
				$pp->toWikidata() ;
			} else {
				$msg = "PagePile is already Wikidata" ;
			}

			addLog ( $msg ) ;
		
		
		} else if ( $f->type == 'filter_namespace' ) {
		
			$keep = array() ;
			$a = explode ( ',' , $f->params[0]->value ) ;
			foreach ( $a AS $v ) {
				if ( preg_match ( '/^\d+/' , $v ) ) $keep[] = $v*1 ;
			}
			
			$remove = array() ;
			$a = explode ( ',' , $f->params[1]->value ) ;
			foreach ( $a AS $v ) {
				if ( preg_match ( '/^\d+/' , $v ) ) $remove[] = $v*1 ;
			}
			
			$pp->filterByNamespace ( $keep , $remove ) ;
			
		
			$msg = "Kept pages in namespaces (".implode(',',$keep)."), removed pages in namespaces (".implode(',',$remove).")";
			addLog ( $msg ) ;
		
		} else if ( $f->type == 'from_wikidata' ) {
		
			$wiki = $f->params[0]->value ;
			$msg = "Switched from Wikidata to $wiki" ;
			if ( $pp->getWiki() != 'wikidatawiki' ) {
				$msg = "PagePile is not Wikidata" ;
			} else {
				$pp->fromWikidata ( $wiki ) ;
			}

			addLog ( $msg ) ;

		} else if ( $f->type == 'no_wikidata' ) {

			$removed = $pp->removePagesWithWikidataItem() ;
			$msg = "Removed $removed pages with Wikidata item, keeping " . $pp->getPageNumber() ;
			addLog ( $msg ) ;
		
		} else if ( $f->type == 'follow_redirects' ) {
		
			$wiki = $f->params[0]->value ;
			$msg = "Following redirects; " . $pp->followRedirects() . " redirects followed." ;
			addLog ( $msg ) ;

		} else if ( $f->type == 'random_subset' ) {
		
			$size = $kv->size ;
			$num = $pp->getPageNumber() ;
			if ( $num <= $size ) {
				addLog ( "Skipped random subset - only $num pages in pile." ) ;
				continue ;
			}
			
			$diff = $num - $size ; // How many to delete
			$sql = "DELETE FROM pages WHERE id IN (select p.id from pages p order by random() limit $diff)" ;
			$sqlite = $pp->getSqlite() ;
			$results = $sqlite->query ( $sql ) ;
			
			addLog ( "Random subset : " . number_format($diff) . " pages removed" ) ;
			
		
		} else {
		
			$out['log'][] = "ERROR: Unknown type '{$f->type}'" ;
			break ;
		
		}


	}
	
} else {

	$out['status'] = "ERROR: Unknown action '$action'" ;

}




header('Content-Type: application/json');
if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback']."(" ;
print json_encode ( $out ) ;
if ( isset($_REQUEST['callback']) ) print ");" ;

?>
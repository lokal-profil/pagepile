<?PHP

/*
TESTING:
gather 6184
paste 83bdeaac
*/

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( "php/common.php" ) ;
require_once ( "php/pagepile.php" ) ;

$testing = isset($_REQUEST['testing']) ;

$oa = new MW_OAuth ( 'pagepile' , 'www' , 'wikimedia' ) ;
$oar = $oauth_pagepile ;

$params = (object) array() ;
$params->language = get_request('language','en') ;
$params->project = get_request('project','wikipedia') ;
$params->action = get_request('action','') ;
$params->to_wikidata = get_request('to_wikidata',false) ;
$params->follow_redirects = get_request('follow_redirects',false) ;


$param_list = array(
	'manual_list' => array() ,
	'sparql' => array() ,
	'pastebin' => array ( 'PasteBin' , 'https://tools.wmflabs.org/paste/' , 'PasteBin ID' ) ,
	'gather' => array ( 'Gather' , 'https://en.m.wikipedia.org/wiki/Special:Gather' , 'Gather ID' ) ,
	'wdq' => array ( 'WDQ' , 'https://wdq.wmflabs.org/api_documentation.html' , 'Wikidata Query' ) ,
	'quarry' => array ( 'Quarry' , 'http://quarry.wmflabs.org/' , 'Quarry ID' , 'Requires page_title, optionally page_namespace. Also requires language/project parameters on top' ) ,
) ;
foreach ( $param_list AS $k => $v ) {
	$params->$k = get_request($k,'') ;
}


if ( $params->language == 'commons' ) $params->project = 'wikimedia' ;
if ( $params->project == 'wikidata' ) $params->language = 'www' ;

function ts_pretty ( $ts ) {
	return substr($ts,0,4).'-'.substr($ts,4,2).'-'.substr($ts,6,2).'&nbsp;'.substr($ts,8,2).':'.substr($ts,10,2).':'.substr($ts,12,2) ;
}

function writeForm() {
	global $params , $param_list , $oa , $oar , $testing ;
	
	print get_common_header ( '' , 'Page Pile' ) ;
	
	if ( $params->action == 'login' ) $oa->doAuthorizationRedirect() ;
	
	$oar = $oa->doIdentify() ;
	if ( $params->action == 'logout' ) {
		$oa->logout() ;
		print "<p>You have logged out of PagePile OAuth.</p>" ;
		$oar = (object) array('is_authorized'=>false) ;
	}
	
	if ( $oar->is_authorized ) {
		print "<div class='lead'>Welcome, {$oar->username}! <small>(you can <a href='?action=logout'>log out here</a>)</small></div>" ;
	} else {
		print "<div class='lead'>This tool works better if you are identified by your Wiki user name. <a href='?action=login'>Log in</a> to identify!</div>" ;
	}

#	print "<pre>" ; print_r ( $r ) ; print "</pre>" ;
	
	$menu = get_request ( 'menu' , '' ) ;

	if ( $menu == 'recent' ) {
		$max = get_request ( 'max' , 15 ) * 1 ;
		$offset = get_request ( 'offset' , 0 ) * 1 ;
		print "<div class='lead'>These are the last $max page piles that were created.</div>" ;
		$pp = new PagePile ;
		$db = $pp->getDB() ;
		$sql = "SELECT * FROM piles ORDER BY created DESC LIMIT $max OFFSET $offset" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		print "<table class='table table-striped'>" ;
		print "<thead><tr><th style='text-align:right'>ID</th><th style='text-align:right'>Pages</th><th>Wiki</th>" ;
		print "<th>User</th>" ;
		print "<th style='text-align:right'>Created</th><th style='text-align:right'>Touched</th></tr></thead><tbody>" ;
		while($o = $result->fetch_object()){
			try {
				$pp2 = new PagePile ($o->id) ;
			} catch ( Exception $e ) {
				print "<tr><td>{$o->id}</td><td colspan=4>unavailable: " . $e->getMessage() . "</td></tr>" ;
				continue ;
			}
			
			$sqlite = $pp2->getSqlite() ;
			if ( !isset($sqlite) ) {
				print "<tr><td>{$o->id}</td><td colspan=4>unavailable</td></tr>" ;
				continue ;
			}
			
			print "<tr>" ;
			print "<td style='text-align:right;font-family:Courier'><a href='./api.php?id={$o->id}&action=get_data&format=html&doit1'>" . $o->id . "</a></td>" ;
			print "<td style='text-align:right;font-family:Courier'>" ;
			print number_format($pp2->getPageNumber()) ;
			print "</td>" ;
			print "<td>" . $pp2->getWiki() . "</td>" ;
			print "<td>" . $o->user . "</td>" ;
			print "<td style='text-align:right'>" . ts_pretty($o->created) . "</td>" ;
			print "<td style='text-align:right'>" . ts_pretty($o->touched) . "</td>" ;
			print "</tr>" ;
		}
		print "</tbody></table>" ;
		
		print "<p><a href='?menu=recent&max=$max&offset=" . ($offset+$max) . "'>Next $max</a></p>" ;
	}
	
	if ( $menu == 'info' ) {
		print "<h2>Get pile info</h2>
		<form class='form form-inline inline-form' method='get' action='api.php'>
		<table class='table'>
		<tr><td colspan=2><input type='number' name='id' placeholder='Pile ID' /></td></tr>
		<tr><td colspan=2><label><input type='radio' name='action' value='get_info' /> Get pile filename</label></td></tr>
		<tr><td><label><input type='radio' name='action' value='get_data' checked /> Get pages in pile</label></td><td>Format:
		<label><input type='radio' name='format' value='html' checked /> HTML</label>
		<label><input type='radio' name='format' value='wiki' /> wikitext</label>
		<label><input type='radio' name='format' value='text' /> plain text</label>
		</td></tr>
		</table>
		<input type='submit' class='btn btn-primary' name='doit' value='Do it!' />
		</form>" ;
	}

	if ( $menu == 'filter' ) {
		$pile = get_request('pile',0)*1 ;
		print "<h2>Filter and combine piles</h2>
		<script>var start_pile = $pile ;</script>
		<script src='./filter.js'></script>
		<style>
		div.filter_wrapper {
			margin:5px;
			padding:3px;
			border:3px solid #EEE;
			font-size:16pt;
			min-height:60px;
		}
		div.filter_part {
			display:inline-block;
			margin-right:10px;
		}
		div.filter_desc {
			font-size:9pt;
		}
		input.badParameter {
			background-color:#FF4848;
		}
		</style>
		<div id='filters'></div>" ;
	}
	
	if ( $menu == 'new' ) {
		print "<h2>Create a new page pile</h2>
		<form class='form form-inline inline-form' method='post' action='index.php'>
	
		<p>
		Language <input type='text' name='language' value='$params->language' />
		project <input type='text' name='project' value='$params->project' />
		<small>use any language for Wikidata; commons/wikimedia for Commons</small>
		</p>
	
		<table class='table'>
		<thead>Fill in one of these:</thead>
		<tbody>
	
		<tr><td nowrap>Manual list</td>
		<td style='width:100%'><textarea placeholder='One (optionally) namespace-prefixed page per line' name='manual_list' rows=3 style='width:100%'>{$params->manual_list}</textarea></td></tr>
		
		<tr><td nowrap><a href='https://query.wikidata.org' target=_blank'>SPARQL</a></td>
		<td style='width:100%'><textarea placeholder='SPARQL query; first result column needs to be Wikidata item' name='sparql' rows=3 style='width:100%'>{$params->sparql}</textarea></td></tr>" ;
	
		foreach ( $param_list AS $k => $v ) {
			if ( count($v) < 3 ) continue ;
			print "<tr><td><a href='{$v[1]}' target='_blank'>{$v[0]}</a></td>" ;
			print "<td><input type='text' name='$k' value='{$params->$k}' placeholder='{$v[2]}' /> <small>{$v[3]}</small></td></tr>" ;
		}
	
		print "
		</tbody></table>
		<div><label><input type='checkbox' name='follow_redirects' value='1' /> Follow (resolve) redirects</label></div>
		<div><label><input type='checkbox' name='to_wikidata' value='1' /> Translate result to Wikidata items immediately</label></div>
		<input type='submit' class='btn btn-primary' name='doit' value='Do it!' />
		</form>" ;
		
		$pp = new PagePile ;
		print "<hr/><h2>Create a new pile in another tool</h2>" ;
		foreach ( $pp->generators AS $tool => $url ) print "<li><a href='$url'>$tool</a></li>" ; // ?pagepile_enabeled=1
		print "</ol></div>" ;
	}
	
	
	if ( $menu == '' ) {
		print "<div class='lead'>PagePile manages lists of pages on Wikipedia, Wikidata, Commons, etc.<br/>Many tools can take PagePile as input, or can generate it as output.</div>" ;
		print "<div>
		<a class='btn btn-primary btn-large' href='?menu=new'>Create a new pile</a>
		<a class='btn btn-primary btn-large' href='?menu=filter'>Filter and combine piles</a>
<!--		<a class='btn btn-primary btn-large' href='?menu=info'>Pile info</a>-->
		<a class='btn btn-primary btn-large' href='?menu=recent'>Recent piles</a>
		</div>" ;
		print "<div style='margin-top:10px'><form action='/pagepile/api.php' method='get' class='form-inline'>
		<input type='hidden' name='action' value='get_data' />
		<input class='input-large' type='number' name='id' placeholder='PagePile ID' />
		<input type='submit' value='Go to pile' class='btn btn-primary btn-large' />
		</div>" ;
		print "<h2>See also</h2><ul>" ;
		print "<li><a href='howto.html'>Programmer's introduction</a></li>" ;
		print "<li><a href='https://bitbucket.org/magnusmanske/pagepile'>Source code</a></li>" ;
		print "<li><a href='https://tools.wmflabs.org/pagepile/doxygen/html/'>Doxygen code documentation</a></li>" ;
		print "</ul>" ;
	}
	
	print get_common_footer() ;
}


function newPileFromGather () {
	global $pp , $params , $out ;
	$pp->createNewPile ( $params->language , $params->project , getUser() ) ;
	$continue = '' ;
	$lspcontinue = '' ;
	while ( 1 ) {
		$url = "https://{$params->language}.{$params->project}.org/w/api.php?action=query&format=json&list=listpages&lsplimit=500&continue={$continue}&lspid=" . preg_replace('/\D/','',$params->gather) ;
		if ( $lspcontinue != '' ) $url .= "&lspcontinue=" . $lspcontinue ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
//		print "<pre>" ; print_r ( $j ) ; print "</pre>" ;
		
		foreach ( $j->query->listpages AS $v ) {
			$ns = $v->ns ;
			$title = $v->title ;
			if ( $ns != 0 ) $title = preg_replace ( '/^[^:]+:/' , '' , $title ) ;
			$pp->addPage ( $title , $ns ) ;
		}

		if ( isset($j->continue) ) {
			$continue = $j->continue->continue ;
			$lspcontinue = $j->continue->lspcontinue ;
		} else break ;
	}
	$o = $pp->getTrack() ;
	$o->action = 'gather' ;
	$o->gather_id = $params->gather ;
	$o->label = "Import from Gather" ;
	$o->url = "https://{$params->language}.m.{$params->project}.org/wiki/Special:Gather/id/{$params->gather}" ;
	$pp->setTrack ( $o ) ;
}

function newPileFromPasteBin () {
	global $pp , $params , $out ;
	$pp->createNewPile ( $params->language , $params->project , getUser() ) ;
	$rows = explode ( "\n" , file_get_contents ( "https://tools.wmflabs.org/paste/view/raw/".$params->pastebin ) ) ;
	foreach ( $rows AS $r ) $pp->addPage ( $r ) ;
	$o = $pp->getTrack() ;
	$o->action = 'pastebin' ;
	$o->pastebin_id = $params->pastebin ;
	$o->label = "Import from PasteBin" ;
	$o->url = "https://tools.wmflabs.org/paste/view/{$params->pastebin}" ;
	$pp->setTrack ( $o ) ;
}

function newPileFromManual () {
	global $pp , $params , $out ;
	$pp->createNewPile ( $params->language , $params->project , getUser() ) ;
	$rows = explode ( "\n" , $params->manual_list ) ;
	foreach ( $rows AS $r ) $pp->addPage ( $r ) ;
	$o = $pp->getTrack() ;
	$o->action = 'manual' ;
	$o->label = "Manually imported page list" ;
	$pp->setTrack ( $o ) ;
}


function newPileFromWDQ () {
	global $pp , $params , $out , $wdq_internal_url ;
	$pp->createNewPile ( 'wikidatawiki','',getUser() ) ;
	$sqlite_file = $pp->getSqliteFile() ;
	$url = "$wdq_internal_url?q=" . urlencode($params->wdq) ;
	
	set_time_limit ( 60 * 10 ) ; // 10 minutes
	$tmp1 = tempnam ( '/shared/' , 'wdq_tmp_' ) ;
	file_put_contents($tmp1, fopen($url, 'r'));
	$cmd = 'cat ' . $tmp1 . ' | sed \'s/[\[,]/\n/g\' | sed \'s/\]/\n/g\'' ;

	$fh = popen ( $cmd , 'r' ) ;
	while ( !feof($fh) ) {
		$q = fgets ( $fh ) ;
		if ( !preg_match ( '/^\d+$/' , $q ) ) continue ;
		$pp->addPage ( "Q$q" , 0 ) ;
	}
	pclose ( $fh ) ;

	exec ( $cmd ) ;
	unlink ( $tmp1 ) ;


	$o = $pp->getTrack() ;
	$o->action = 'wdq' ;
	$o->wdq = $params->wdq ;
	$o->label = "Wikidata Query" ;
	$o->url = $url ;
	$pp->setTrack ( $o ) ;
}


function newFromQuarry () {
	global $pp , $params , $out ;
	$url = "https://quarry.wmflabs.org/query/" . $params->quarry . "/result/latest/0/json" ;
	$f = file_get_contents ( $url ) ;
	if ( $f === false or $f == '' ) die ( "The quarry URL $url does not work." ) ;
	$j = json_decode ( $f ) ;
	if ( $j === null ) die ( "Invalid JSON in $url" ) ;
	
	$col = array() ;
	foreach ( $j->headers AS $k => $v ) $col[$v] = $k ;
	if ( !isset ( $col['page_title'] ) ) die ( "page_title column required (page_namespace would be nice too)" ) ;
	
	$pp->createNewPile ( $params->language , $params->project , getUser() ) ;
	foreach ( $j->rows AS $v ) {
		$page = $v[$col['page_title']] ;
		$ns = 0 ; // Default; -999 would be auto-detect
		if ( isset ( $col['page_namespace'] ) ) $ns = $v[$col['page_namespace']] ;
		$pp->addPage ( $page , $ns ) ;
	}
	$pp->commitTransaction() ;

	$o = $pp->getTrack() ;
	$o->action = 'quarry' ;
	$o->label = "Imported from Quarry #" . $params->quarry ;
	$pp->setTrack ( $o ) ;
}

function newPileFromSPARQL () {
	global $pp , $params , $out ;
	$url = "https://query.wikidata.org/bigdata/namespace/wdq/sparql?format=json&query=".urlencode($params->sparql) ;
	$f = file_get_contents ( $url ) ;
	if ( $f === false or $f == '' ) die ( "The SPARQL URL $url does not work." ) ;
	$j = json_decode ( $f ) ;
	if ( $j === null ) die ( "Invalid JSON in $url" ) ;

	$key = $j->head->vars[0] ; // First column key
	$pp->createNewPile ( 'wikidata' , 'wikidata' , getUser() ) ;
	foreach ( $j->results->bindings AS $v ) {
		$v0 = $v->$key ;
		if ( !preg_match ( '/www\.wikidata\.org\/entity\/(Q\d+)$/' , $v0->value , $m ) ) continue ;
		$pp->addPage ( $m[1] , 0 ) ;
	}
		
	$pp->commitTransaction() ;

	$o = $pp->getTrack() ;
	$o->action = 'sparql' ;
	$o->label = "Imported from SPARQL query" ;
	$pp->setTrack ( $o ) ;
}

function getUser() {
	global $oar ;
	if ( $oar->is_authorized ) return $oar->username ;
	return '' ;
}

if ( isset($_REQUEST['doit']) ) {


	$pp = new PagePile ;

	$out = array ( 'status' => 'OK' ) ;
	if ( $params->gather != '' ) newPileFromGather() ;
	else if ( $params->pastebin != '' ) newPileFromPasteBin() ;
	else if ( $params->wdq != '' ) newPileFromWDQ() ;
	else if ( $params->sparql != '' ) newPileFromSPARQL() ;
	else if ( $params->quarry != '' ) newFromQuarry() ;
	else if ( $params->manual_list != '' ) newPileFromManual() ;
	else {
		$out['status'] = 'ERROR: No input data' ;
	}
	
	
	if ( $out['status'] == 'OK' ) {
		if ( $params->follow_redirects ) $pp->followRedirects() ;
		if ( $params->to_wikidata ) $pp->toWikidata() ;
		$pp->printAndEnd() ;
	}
	
	header('Content-Type: application/json');
	if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback']."(" ;
	print json_encode ( $out ) ;
	if ( isset($_REQUEST['callback']) ) print ");" ;
	
} else {
	writeForm() ;
}

?>